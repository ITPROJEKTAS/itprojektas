<?php
require "./components/header.php"
?>
<!-- Paveikslėliai -->
<div class="slideshow-container">
  <div class="mySlides fade">
    <div class="numbertext">1 / 3</div>
    <img src="./img/blur-breakfast-close-up-376464.jpg" style="width:100%">
    <div class="text">Breakfast</div>
  </div>

  <div class="mySlides fade">
    <div class="numbertext">2 / 3</div>
    <img src="./img/burger-chips-dinner-70497.jpg" style="width:100%">
    <div class="text">Burger & chips Two</div>
  </div>

  <div class="mySlides fade">
    <div class="numbertext">3 / 3</div>
    <img src="./img/burrito-chicken-close-up-461198.jpg" style="width:100%">
    <div class="text">Burrito chicken</div>
  </div>
</div>
<br>
<!-- Taškai po paveikslėliais -->
<div style="text-align:center">
  <span class="dot"></span> 
  <span class="dot"></span> 
  <span class="dot"></span> 
</div>
<!-- scriptas slideshow, verčia nuotraukas keistis (javascript) -->
<?php include "./scripts/slideshow.php"?>

<?php
  require "./components/footer.php"
?>