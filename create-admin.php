<?php
include("process.php");
require 'components/admin-header.php';
?>
<div class="content">
        <h2>Prideti administratoriu!</h2>
        <form method="POST" class="register" action="signin.php">
            <input type="text" name="user" id="userFirstName" placeholder="First name" required>
            <input type="text" name="usurname" id="userLastName" placeholder="Last name" required>
            <p></p>
            <input type="email" name="mail" id="userEmail" placeholder="Email" required>
            <input type="password" name="pass" id="userPassword" placeholder="Password" required>
            <p></p>
            <input type="tel" name="mobile" id="userPhoneNumber" placeholder="Phone number" required>
            <input type="text" name="address" id="userAddress" placeholder="Your address" required>
            <p></p>
            <input type="text" name="asmkodas" id="userPersonalCode" placeholder="Asmens kodas" required>
            <p></p>
            <!-- <input type="submit" id="register"> -->
            <button type="submit" id="registerSubmit" name="register-admin">Register</button>
        </form>
</div>
<?php
    require 'components/footer.php'
?>