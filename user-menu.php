<?php
    require 'components/user-header.php'
?>
<div class="foodMenu">
    <h2>Maisto meniu</h2>
    <?php 
    $db=mysqli_connect('localhost', 'root', '', 'ispprojektas');

    $SQLquery = "SELECT * FROM meniu";
    $resultatas = mysqli_query($db, $SQLquery);
    while($rows = mysqli_fetch_array($resultatas)) 
    {
        $id = $rows['ID'];
        $pav = "Kiti patiekalai:";
        if($id == 1)
            $pav = "Karšti patiekalai:";
        if($id == 2)
            $pav = "Gėrimų meniu:";
        echo "
        <div class='dishesMeniu'>
            <h3>
                $pav
            </h3>
        </div>
        <div class='dishesFL'>
            <div class='foodName'>
                Pavadinimas
            </div>
            <div class='foodDetails'>
                Aprašas
            </div>
            <div class='foodPrice'>
                Kaina
            </div>
        </div>
    ";
        $query = "SELECT * FROM patiekalas WHERE MeniuID = '$id'";
        $results = mysqli_query($db, $query);
        while($rows = mysqli_fetch_array($results)) {

            $pavadinimas = $rows['Pavadinimas'];
            $kaina = $rows['Kaina'];
            $aprasymas = $rows['Aprasymas'];
            $kolorijos = $rows['Kolorijos'];
            $alergenai = $rows['Alegenai'];
            $sudetis = $rows['Sudetis'];
            echo "
            <div class='dishes'>
                <div class='foodName'>
                    $pavadinimas
                </div>
                <div class='foodDetails'>
                    $aprasymas
                </div>
                <div class='foodPrice'>
                    $kaina €
                </div>
            </div>
            ";
         }
    }

    ?>
</div>
<?php
    require 'components/footer.php'
?>