<?php
include("process.php");
require 'components/header.php'
?>
<div class="content">
    <div class="login">
        <form action="signin.php" class="login" method="POST">
            <p>e-paštas:</p>
            <input type="email" name = "mail" id="userEmail" placeholder="įveskite e-paštą" required>
            <p>slaptažodis:</p>
            <input type="password" name = "pass" id="userPassword" placeholder="Įveskite slaptažodį" required><br>
            <input style="color: red;" type="checkbox" name="admin" value="Admin"> Administratorius?
            <button type="submit" routerLink="/user-main.html" id="loginSubmit" name="login_user">Login</button><br>
            <a href="#">Pamiršote slaptašodį?</a><br>
            
        </form>
    </div>
    <div class="register">
        <h2>Do not have an account? register now!</h2>
        <form method="POST" class="register" action="signin.php">
            <input type="text" name="user" id="userFirstName" placeholder="First name" required>
            <input type="text" name="usurname" id="userLastName" placeholder="Last name" required>
            <p></p>
            <input type="email" name="mail" id="userEmail" placeholder="Email" required>
            <input type="password" name="pass" id="userPassword" placeholder="Password" required>
            <p></p>
            <input type="tel" name="mobile" id="userPhoneNumber" placeholder="Phone number" required>
            <input type="text" name="address" id="userAddress" placeholder="Your address" required>
            <p></p>
            <label for="lytis"> Jūs esate </label>
            <input name='gender' type="radio" value="1"> Vyras
            <input name='gender' type="radio" value="2"> Moteris
            <p></p>
            <label for="birthday">Birthday</label>
            <input type="date" name="bday">
            <p></p>
            <!-- <input type="submit" id="register"> -->
            <button type="submit" routerLink="/user-main.html" id="registerSubmit" name="register">Register</button>
        </form>
    </div>
</div>
<?php
    require 'components/footer.php'
?>