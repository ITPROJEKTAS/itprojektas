<?php
session_start();

$errors = array();

$db=mysqli_connect('localhost', 'root', '', 'ispprojektas');


//if register button is clicked
if(isset($_POST['register'])){
	$user=$_POST['user'];
	$usurname=$_POST['usurname'];
	$pass=$_POST['pass'];
	$mail=$_POST['mail'];
	$gender=$_POST['gender'];
	$birthday=$_POST['bday'];
	$phoneno=$_POST['mobile'];
	$adress=$_POST['address'];

  //to ensure that fields are filled
	if(empty($user)) {
		array_push($errors, "Privaloma ivesti varda");
	}
	if(empty($usurname)) {
		array_push($errors, "Privaloma ivesti pavarde");
	}
	if(empty($pass)) {
		array_push($errors, "Privaloma ivesti slaptazodi");
	}
	if(empty($mail)) {
		array_push($errors, "Privaloma ivesti e-pasta");
	}
	if(empty($birthday)) {
		array_push($errors, "Privaloma ivesti gimimo data");
	}
	if(empty($gender)) {
		array_push($errors, "Privaloma pasirinkti lyti");
	}
	if(empty($phoneno)) {
		array_push($errors, "Privaloma irasyti telefono numeri");
    }

	if(count($errors)==0) {
	//$pass=substr(hash('sha256',$pass),5,32);
	//$pass=substr(hash('sha256',$pass),5,32);
	$sql = "INSERT INTO registruotasklientas (Vardas, Pavarde, Telefono_numeris,El_Pastas,Adresas,Reklama,Nuotrauka,Gimimo_data,Lytis,Slaptazodis) 
					VALUES ('$user','$usurname', '$phoneno', '$mail','$adress','1','null', '$birthday', '$gender', '$pass')";
	mysqli_query($db, $sql);

	$_SESSION['success'] = "Sekmingai uzsiregistravote.";
	$_SESSION['name']=$user;
	$_SESSION['surname']=$usurname;
	$_SESSION['pass']=$pass;
	$_SESSION['mail']=$mail;
	$_SESSION['gender']=$gender;
	$_SESSION['bday']=$birthday;
	$_SESSION['mobile']=$phoneno;
	$_SESSION['address']=$adress;

	header('location: user-main.php');

	}
}

if (isset($_POST['login_user'])) {

  $pass = $_POST['pass'];
  $email = $_POST['mail'];
	$check = $_POST['admin'];
	if (empty($email)) {
		array_push($errors, "El. pastas privalomas");
	}
	if (empty($pass)) {
		array_push($errors, "Slaptazodis privalomas");
	}
	if (count($errors) == 0) {
		if ($check == 'Admin'){
			$query = "SELECT * FROM darbuotojas WHERE El_Pastas='$email' AND Slaptazodis='$pass'";
			$results = mysqli_query($db, $query);
		
		if (mysqli_num_rows($results) > 0) {
			while($row3 = mysqli_fetch_array($results))
			{
				$_SESSION['name'] = $row3['Vardas'];
				$_SESSION['mail'] = $email;
				$_SESSION['surname']=$row3['Pavarde'];
				$_SESSION['pass']=$pass;
				$_SESSION['gender']=$row3['Lytis'];
				$_SESSION['bday']=$row3['Gimimo_Data'];
				$_SESSION['mobile']=$row3['Telefono_Numeris'];
				$_SESSION['address']=$row3['Adresas'];
				$_SESSION['asm_kodas']=$row3['Asmens_Kodas'];
				$_SESSION['tipas']=$check;
				$_SESSION['success'] = "Sekmingai uzsiregistravote.";	
			}
			  $_SESSION['success'] = "You are now logged in";
				header('location: admin-main.php');
		}
	}
		else {
			$query = "SELECT * FROM registruotasklientas WHERE El_Pastas='$email' AND Slaptazodis='$pass'";
			$results = mysqli_query($db, $query);
		
		if (mysqli_num_rows($results) > 0) {
			while($row3 = mysqli_fetch_array($results))
			{
				$_SESSION['id'] = $row3['ID'];
				$_SESSION['name'] = $row3['Vardas'];
				$_SESSION['mail'] = $email;
				$_SESSION['surname']=$row3['Pavarde'];
				$_SESSION['pass']=$pass;
				$_SESSION['gender']=$row3['Lytis'];
				$_SESSION['bday']=$row3['Gimimo_Data'];
				$_SESSION['mobile']=$row3['Telefono_Numeris'];
				$_SESSION['address']=$row3['Adresas'];
				$_SESSION['tipas']=$check;

				$_SESSION['success'] = "Sekmingai uzsiregistravote.";	
			}

			$_SESSION['success'] = "You are now logged in";
	  	header('location: user-main.php');
		}	
		}
		}else {
		array_push($errors, "Wrong username/password combination");
	}
}
	
	if (isset($_POST['update'])) {
		$pass=$_SESSION['pass'];
		$mail=$_SESSION['mail'];
		$query = "SELECT * FROM registruotasklientas WHERE El_Pastas='$mail' AND Slaptazodis='$pass'";
		$results = mysqli_query($db, $query);

		
		$rows = mysqli_fetch_array($results);


		$u=$_POST['user'];
		$us=$_POST['usurname'];
		$p=$_POST['pass'];
		$ph=$_POST['mobile'];
		$a=$_POST['address'];

		if(empty($u)) {
			$u=$rows['Vardas'];
		}
		if(empty($us)) {
			$us=$rows['Pavarde'];
		}
		if(empty($p)) {
			$p=$rows['Slaptazodis'];
		}
		
		if(empty($ph)) {
			$ph=$rows['Telefono_Numeris'];
		}
		if(empty($a)) {
			$a=$rows['Adresas'];
		}

		
		$quer = "UPDATE registruotasklientas SET Vardas='$u', Pavarde='$us', Slaptazodis = '$p', Telefono_Numeris='$ph', Adresas='$a' WHERE El_Pastas = '$mail' AND Slaptazodis='$pass'";
		$rez = mysqli_query($db, $quer);

		$_SESSION['success'] = "Duomenys atnaujinti sėkmingai.";	
		$_SESSION['name']=$u;
		$_SESSION['surname']=$us;
		$_SESSION['pass']=$p;
		$_SESSION['mobile']=$ph;
		$_SESSION['address']=$a;

		header('location: user-profile.php');
	}


	if (isset($_POST['updateA'])) {
		$pass=$_SESSION['pass'];
		$mail=$_SESSION['mail'];
		$query = "SELECT * FROM darbuotojas WHERE El_Pastas='$mail' AND Slaptazodis='$pass'";
		$results = mysqli_query($db, $query);

		
		$rows = mysqli_fetch_array($results);


		$u=$_POST['user'];
		$us=$_POST['usurname'];
		$p=$_POST['pass'];
		$ph=$_POST['mobile'];
		$a=$_POST['address'];

		if(empty($u)) {
			$u=$rows['Vardas'];
		}
		if(empty($us)) {
			$us=$rows['Pavarde'];
		}
		if(empty($p)) {
			$p=$rows['Slaptazodis'];
		}
		
		if(empty($ph)) {
			$ph=$rows['Telefono_Numeris'];
		}
		if(empty($a)) {
			$a=$rows['Adresas'];
		}

		
		$quer = "UPDATE darbuotojas SET Vardas='$u', Pavarde='$us', Slaptazodis = '$p', Telefono_Numeris='$ph', Adresas='$a' WHERE El_Pastas = '$mail' AND Slaptazodis='$pass'";
		$rez = mysqli_query($db, $quer);

		$_SESSION['success'] = "Duomenys atnaujinti sėkmingai.";	
		$_SESSION['name']=$u;
		$_SESSION['surname']=$us;
		$_SESSION['pass']=$p;
		$_SESSION['mobile']=$ph;
		$_SESSION['address']=$a;

		header('location: admin-profile.php');
	}

	
	if (isset($_POST['update-staff'])) {
		
		$mail=$_POST['mail'];
		$query = "SELECT * FROM darbuotojas WHERE El_Pastas='$mail'";
		$results = mysqli_query($db, $query);

		
		$rows = mysqli_fetch_array($results);
		

		$u=$_POST['user'];
		$us=$_POST['usurname'];
		$pr=$_POST['pareigos'];
		$ph=$_POST['mobile'];
		$a=$_POST['address'];

		if(empty($u)) {
			$u=$rows['Vardas'];
		}
		if(empty($us)) {
			$us=$rows['Pavarde'];
		}
		
		if(empty($ph)) {
			$ph=$rows['Telefono_Numeris'];
		}
		if(empty($pr)) {
			$pr=$rows['Pareigos'];
		}
		if(empty($a)) {
			$a=$rows['Adresas'];
		}

		
		
		$quer = "UPDATE darbuotojas SET Vardas='$u', Pavarde='$us', Telefono_Numeris='$ph',Pareigos='$pr', Adresas='$a' WHERE El_Pastas = '$mail'";
		$rez = mysqli_query($db, $quer);
		
		header('location: admin-all-staff.php');
		
	}


	if (isset($_POST['update-food'])) {
		
		$pav=$_POST['pavID'];
		$query = "SELECT * FROM patiekalas WHERE Pavadinimas='$pav'";
		$results = mysqli_query($db, $query);

		
		$rows = mysqli_fetch_array($results);
		

		$u=$_POST['pav'];
		$us=$_POST['kaina'];
		$pr=$_POST['apr'];
		$ph=$_POST['kal'];
		$a=$_POST['aleg'];

		if(empty($u)) {
			$u=$rows['Pavadinimas'];
		}
		if(empty($us)) {
			$us=$rows['Kaina'];
		}
		
		if(empty($ph)) {
			$ph=$rows['Aprasymas'];
		}
		if(empty($pr)) {
			$pr=$rows['Kolorijos'];
		}
		if(empty($a)) {
			$a=$rows['Alegenai'];
		}

		
		
		$quer = "UPDATE patiekalas SET Pavadinimas='$u', Kaina='$us', Aprasymas='$ph',Kolorijos='$pr', Alegenai='$a' WHERE Pavadinimas = '$pav'";
		$rez = mysqli_query($db, $quer);
		
		header('location: admin-menu.php');
		
	}

	if (isset($_POST['update-reserv'])) {
		
		$ID=$_POST['laikasID'];
		$query = "SELECT * FROM rezervacija WHERE ID='$ID'";
		$results = mysqli_query($db, $query);

		
		$rows = mysqli_fetch_array($results);
		

		$u=$_POST['laikas'];

		if(empty($u)) {
			$u=$rows['Laikas'];
		}

		echo $u;
		
		$quer = "UPDATE rezervacija SET Laikas='$u' WHERE ID = '$ID'";
		$rez = mysqli_query($db, $quer);
		
		header('location: admin-all-reservations.php');
		
	}

	if (isset($_POST['update-uzmaist'])) {
		
		
		$ID=$_POST['UID'];
		echo $ID;
		$query = "SELECT * FROM uzsakymas_inamus WHERE ID='$ID'";
		$results = mysqli_query($db, $query);

		
		$rows = mysqli_fetch_array($results);
		

        $busena = $_POST['busena'];
        $adresas =  $_POST['adr'];
        $komentaras = $_POST['kom'];
        $laikasPr = $_POST['laikas'];

		if(empty($busena)) {
			$busena= $rows['Busena'];
		}
		if(empty($adresas)) {
			$adresas=$rows['Adresas'];
		}
		if(empty($komentaras)) {
			$komentaras=$rows['Komentaras']; 
		}
		if(empty($laikasPr)) {
			$laikasPr=$rows['Laikas_Pristatymui'];
		}

		
		
		$quer = "UPDATE uzsakymas_inamus SET Busena='$busena',Adresas='$adresas',Komentaras='$komentaras',Laikas_Pristatymui='$laikasPr' WHERE ID = '$ID'";
		$rez = mysqli_query($db, $quer);
		
		header('location: admin-Uzsakymas.php');
		
	}

	if (isset($_POST['delete_reservation'])) {

		$ID=$_SESSION['stid'];
		$vietusk = $_SESSION['vietusk'];
		$laikas = $_SESSION['laikas'];
		$sql = "DELETE FROM rezervacija WHERE ID='$ID'";
		if ($db->query($sql) === TRUE) {
			echo "Record deleted successfully";
		} else {
			echo "Error deleting record: " . $db->error;
		}
		header('location: admin-all-reservations.php');
	}

	if (isset($_POST['update_reservation'])) {

		$ID=$_SESSION['stid'];
		$query = "SELECT * FROM rezervacija WHERE ";
		$results = mysqli_query($db, $query);

		$vt = $_SESSION['vietusk'];
		$laik = $_SESSION['laikas'];

		$quer = "UPDATE rezervacija SET Vietu_Skaicius='$vt', Laikas='$laik' WHERE id='$ID'";
		$rez = mysqli_query($db, $quer);

		$_SESSION['success'] = "Duomenys atnaujinti sėkmingai.";	

		header('location: admin-all-reservations.php');
	}

	if (isset($_POST['delete_member'])) {

		$ID=$_SESSION['id'];

		$sql = "DELETE FROM darbuotojas WHERE id='$ID'";
		if ($db->query($sql) === TRUE) {
			echo "Record deleted successfully";
		} else {
			echo "Error deleting record: " . $db->error;
		}
		header('location: admin-all-staff.php');
	}

	if (isset($_POST['update_member'])) {

		$ID=$_SESSION['id'];
		$query = "SELECT * FROM darbuotojas WHERE id='$ID'";
		$results = mysqli_query($db, $query);
		
		$vardas = $POST['vardas'];
		$pav = $_POST['pavarde'];
		$tel = $_POST['tel'];
		$adr = $_POST['adr'];
		$par = $_POST['pareigos'];

		$quer = "UPDATE darbuotojas SET Vardas='$vardas', Pavarde='$pav', Telefono_Numeris='$tel', Adresas='$adr', Pareigos='$par' WHERE id='$ID'";
		$rez = mysqli_query($db, $quer);

		$_SESSION['success'] = "Duomenys atnaujinti sėkmingai.";	

		header('location: admin-all-staff.php');
	}

	if (isset($_POST['delete_customer'])) {

		$ID=$_SESSION['id'];

		$sql = "DELETE FROM registruotasklientas WHERE id='$ID'";
		if ($db->query($sql) === TRUE) {
			echo "Record deleted successfully";
		} else {
			echo "Error deleting record: " . $db->error;
		}
		header('location: admin-all-users.php');
	}

	if (isset($_POST['update_customer'])) {

		$ID=$_SESSION['id'];
		$query = "SELECT * FROM registruotasklientas WHERE id='$ID'";
		$results = mysqli_query($db, $query);
		
		$vardas = $POST['vardas'];
		$pav = $_POST['pavarde'];
		$tel = $_POST['tel'];
		$adr = $_POST['adr'];

		$quer = "UPDATE darbuotojas SET Vardas='$vardas', Pavarde='$pav', Telefono_Numeris='$tel', Adresas='$adr' WHERE id='$ID'";
		$rez = mysqli_query($db, $quer);

		$_SESSION['success'] = "Duomenys atnaujinti sėkmingai.";	

		header('location: admin-all-users.php');
	}

	if(isset($_POST['register-admin'])){
		$user=$_POST['user'];
		$usurname=$_POST['usurname'];
		$pass=$_POST['pass'];
		$mail=$_POST['mail'];
		$asmkodas=$_POST['asmkodas'];
		$phoneno=$_POST['mobile'];
		$adress=$_POST['address'];
	
	  //to ensure that fields are filled
		if(empty($user)) {
			array_push($errors, "Privaloma ivesti varda");
		}
		if(empty($usurname)) {
			array_push($errors, "Privaloma ivesti pavarde");
		}
		if(empty($pass)) {
			array_push($errors, "Privaloma ivesti slaptazodi");
		}
		if(empty($mail)) {
			array_push($errors, "Privaloma ivesti e-pasta");
		}
		if(empty($asmkodas)) {
			array_push($errors, "Privaloma ivesti asmens koda");
		}
		if(empty($phoneno)) {
			array_push($errors, "Privaloma irasyti telefono numeri");
		}
	
		if(count($errors)==0) {
		$sql = "INSERT INTO darbuotojas (Vardas, Pavarde, Slaptazodis, Telefono_numeris, El_Pastas,Adresas,Asmens_Kodas, Pareigos, RestoranasID) 
						VALUES ('$user','$usurname', '$pass', '$phoneno', '$mail','$adress','$asmkodas', 'administratorius', '1')";
		mysqli_query($db, $sql);
	
	
		header('location: admin-main.php');
	
		}
	}

	if(isset($_POST['add_food'])){
		$pav=$_POST['pavadinimas'];
		$kaina=$_POST['kaina'];
		$apr=$_POST['aprasymas'];
		$kcal=$_POST['kalorijos'];
		$alergenai=$_POST['alergenai'];
		$sudetis=$_POST['sudetis'];

	
	  //to ensure that fields are filled
		if(empty($pav)) {
			array_push($errors, "Privaloma ivesti pavadinima");
		}
		if(empty($kaina)) {
			array_push($errors, "Privaloma ivesti kaina");
		}
		if(empty($apr)) {
			array_push($errors, "Privaloma ivesti aprasyma");
		}
		if(empty($kcal)) {
			array_push($errors, "Privaloma ivesti kaloriju kieki");
		}
		if(empty($alergenai)) {
			array_push($errors, "Privaloma ivesti alergenus");
		}
		if(empty($sudetis)) {
			array_push($errors, "Privaloma ivesti sudeti");
		}
	
		if(count($errors)==0) {
		$sql = "INSERT INTO patiekalas (Pavadinimas, Kaina, Aprasymas, Kolorijos, Alegenai, Sudetis, MeniuID) 
						VALUES ('$pav', '$kaina', '$apr', '$kcal', '$alergenai', '$sudetis','1')";
		mysqli_query($db, $sql);
	
	
		header('location: admin-main.php');
	
		}
		else { echo "nera";}
	}
?>
