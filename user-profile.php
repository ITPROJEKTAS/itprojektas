<?php
    include("process.php");
    require 'components/user-header.php';
?>
<?php
    $mail=$_SESSION['mail'];
    $user=$_SESSION['name'];
    $surname=$_SESSION['surname'];
    $pass=$_SESSION['pass'];
    $phone=$_SESSION['mobile'];
    $address=$_SESSION['address'];
?>
<div class="itemRow">
    <img src="img/Funny and crazy smile.jpg" alt="Avatar" class="avatar">
</div>
    

<div class="container">
    <form method="POST" class="update" action="user-profile.php">
        <div class="itemRow">
            <label for="user">Vardas</label>
            <input type="text" name="user" id="userFirstName" placeholder="<?php echo $user ?>" >
            <label for="usurname">Pavardė</label>
            <input type="text" name="usurname" id="userLastName" placeholder="<?php echo $surname ?>" >
        </div>
        <div class="itemRow">
            <label for="mobile">Telefono numeris</label>
            <input type="tel" name="mobile" id="userPhoneNumber" placeholder="<?php echo $phone ?>" >
            <label for="address">Adresas</label>
            <input type="text" name="address" id="userAddress" placeholder="<?php echo $address ?>" >
        </div>
        <div class="itemRow">
            <label for="pass">Slaptažodis</label>
            <input type="password" name="pass" id="userPassword" placeholder="*******" >
        </div>
        <div class="itemRow">
            <button type="submit" name='update' value='Redaguoti' class="button button1">Redaguoti</button>
        </div>
    </form>
</div>
<?php
    require 'components/footer.php';
?>