<?php
include("process.php");
require 'components/admin-header.php';
?>
<div class="content">
        <h2>Prideti patiekala!</h2> <p>
        <form method="POST" action="add-food.php">
            <input type="text" name="pavadinimas" id="pavadinimas" placeholder="Pavadinimas" required>
            <input type="type="number" step="any" min="0" name="kaina" id="kaina" placeholder="Kaina" required>
            <p></p>
            <input type ="text" name="aprasymas" placeholder="Aprasymas" required>
            <input type="number" name="kalorijos" id="kcal" placeholder="kalorijos" required>
            <p></p>
            <input type="text" name="alergenai" id="alergies" placeholder="Alergenai" required>
            <input type="text" name="sudetis" id="sudetis" placeholder="Sudetis" required>
            <p></p>
            <button type="submit" name="add_food">Prideti</button>
        </form>
</div>
<?php
    require 'components/footer.php'
?>